import React, { Component } from 'react';
import {
  Picker,
  StyleSheet
} from 'react-native';

class Operacao extends Component {
  render() {
    return (
      <Picker
        selectedValue={this.props.operacao}
        onValueChange={(op) => this.props.atualizaOperacao(op)}
        style={styles.operacao}>
        <Picker.Item label='Soma' value='soma' />
        <Picker.Item label='Subtracao' value='subtracao' />
      </Picker>
    );
  }
}

const styles = StyleSheet.create({
  operacao: {
    marginTop: 15,
    marginBottom: 15
  }
});

export { Operacao };