import React  from 'react';
import { View } from 'react-native';

import Calcular from './Calcular';

const Comando = (props) => (
  <View>
    <Calcular acao={props.acao}/>
  </View>
);

export { Comando };